const User = require("../models/User");
const { StatusCodes } = require("http-status-codes");
const { BadRequestError } = require("../errors/BadRequestError");
const { UnAuthenticatedError } = require("../errors/UnAuthenticatedError");

module.exports.register = async (req, res) => {
    const user = await User.create(req.body);
    const token = user.generateToken();
    return res.status(StatusCodes.CREATED).json({
        status: true,
        message: "Successfully register user!",
        entity: {
            user,
            token,
        },
    });
};

module.exports.login = async (req, res) => {
    const { email, password } = req.body;

    if (!email || !password) {
        throw new BadRequestError("Email and password is mandatory!");
    }

    const user = await User.findOne({
        email: email,
    });

    const isValidPassword = await user.validatePassword(password);

    if (!isValidPassword) throw new UnAuthenticatedError("Unauthorize user!");

    const token = user.generateToken();

    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully login user!",
        entity: {
            user,
            token,
        },
    });
};
