const Job = require("../models/Job");
const { StatusCodes } = require("http-status-codes");
const { NotFoundError } = require("../errors/NotFoundError");
const { BadRequestError } = require("../errors/BadRequestError");

module.exports.getAllJobs = async (req, res) => {
    const allJob = await Job.find({
        userId: req.user.userId,
    }).sort("createdAt");

    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully get all jobs!",
        entity: {
            count: allJob.length,
            rows: allJob,
        },
    });
};

module.exports.createJob = async (req, res) => {
    const { name, position, status } = req.body;
    const userId = req.user.userId;
    const job = await Job.create({
        name,
        position,
        status,
        userId,
    });
    return res.status(StatusCodes.CREATED).json({
        status: true,
        message: "SUccessfully created job!",
        entity: job,
    });
};

module.exports.getJob = async (req, res) => {
    const {
        params: { id },
        user: { userId },
    } = req;
    const job = await Job.findOne({
        _id: id,
        userId: userId,
    });

    if (!job) throw new NotFoundError(`No job found with that ${id}`);

    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully get job!",
        entity: job,
    });
};

module.exports.updateJob = async (req, res) => {
    const {
        params: { id },
        user: { userId },
        body: { name, position, status },
    } = req;

    if (!name || !position) throw new BadRequestError("Name and position is mandatory!");

    const job = await Job.findByIdAndUpdate(
        {
            _id: id,
            userId: userId,
        },
        {
            name,
            position,
            status,
        },
        {
            new: true,
            runValidators: true,
        }
    );

    if (!job) throw new NotFoundError(`No job found with ${id}`);

    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully update job!",
        entity: job,
    });
};

module.exports.deleteJob = async (req, res) => {
    const {
        params: { id },
        user: { userId },
    } = req;

    const job = await Job.findByIdAndRemove({
        _id: id,
        userId: userId,
    });

    if (!job) throw new NotFoundError(`No job found with id ${id}`);

    return res.status(StatusCodes.OK).json({
        status: true,
        message: "Successfully delete the job!",
        entity: job,
    });
};
