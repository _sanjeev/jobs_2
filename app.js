const express = require("express");
const dotenv = require("dotenv");
const connectDB = require("./db/connect");
const cors = require("cors");
const helmet = require("helmet");
const xss = require("xss-clean");
const rateLimiter = require("express-rate-limit");

const swaggerUI = require("swagger-ui-express");
const YAML = require("yamljs");
const swaggerDocument = YAML.load("./swagger.yaml");

const { notFound } = require("./middleware/notFound");
const { errorHandler } = require("./middleware/errorHandler");
dotenv.config();
require("express-async-errors");

const app = express();

//middleware
app.use(
    cors()
    //    {
    //     origin: "http://jobs-2.onrender.com",
    // }
);
app.set("trust proxy", 1);
app.use(
    rateLimiter({
        windowMs: 60 * 1000,
        max: 60,
    })
);

app.use(express.json());

app.use(helmet());

app.use(xss());

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.get("/", (req, res) => {
    return res.send("Job Api");
});

app.use("/", require("./routes"));
app.use(notFound);
app.use(errorHandler);

const port = process.env.PORT || 5000;

const start = async () => {
    try {
        await connectDB(process.env.MONGO_URI);
        app.listen(port, (err) => {
            if (err) {
                console.log("Error ", err);
                return;
            }
            console.log(`Server is listening on port ${port}`);
        });
    } catch (error) {
        console.error(error);
    }
};

start();
