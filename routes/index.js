const express = require("express");
const { auth } = require("../middleware/auth");
const router = express.Router();

router.use("/api/v1", require("./AuthRoute"));
router.use("/api/v1", auth, require("./JobRoute"));

module.exports = router;
