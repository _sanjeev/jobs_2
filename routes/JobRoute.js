const express = require("express");
const { createJob, getAllJobs, getJob, updateJob, deleteJob } = require("../controllers/jobs");
const router = express.Router();

router.post("/job", createJob);
router.get("/jobs", getAllJobs);
router.get("/job/:id", getJob);
router.patch("/job/:id", updateJob);
router.delete("/job/:id", deleteJob);

module.exports = router;
