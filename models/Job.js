const mongoose = require("mongoose");

const JOBSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
            required: [true, "Company Name is mandatory!"],
            maxLegth: 50,
        },
        position: {
            type: String,
            trim: true,
            required: [true, "Position is mandatory!"],
            maxLegth: 100,
        },
        status: {
            type: String,
            enum: ["interview", "declined", "pending"],
            default: "pending",
        },
        userId: {
            type: mongoose.Types.ObjectId,
            ref: "User",
            required: [true, "Please provide userId!"],
        },
    },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Job", JOBSchema);
