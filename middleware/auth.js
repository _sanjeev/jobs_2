const { StatusCodes } = require("http-status-codes");
const { UnAuthenticatedError } = require("../errors");
const jwt = require("jsonwebtoken");

module.exports.auth = (req, res, next) => {
    const authHeaders = req.headers.authorization;

    if (!authHeaders || !authHeaders.startsWith("Bearer ")) {
        throw new UnAuthenticatedError("Unauthorize user");
    }

    const token = authHeaders.split(" ")[1];

    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        req.user = {
            userId: decoded.id,
            name: decoded.name,
            email: decoded.email,
        };
        next();
    } catch (error) {
        throw new UnAuthenticatedError("Unauthorize user");
    }
};
