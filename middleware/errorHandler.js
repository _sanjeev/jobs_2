const { StatusCodes } = require("http-status-codes");
const { CustomApiError } = require("../errors");

module.exports.errorHandler = (err, req, res, next) => {
    let customError = {
        status: err.status || false,
        statusCode: err.statusCode || StatusCodes.INTERNAL_SERVER_ERROR,
        message: err.message || "Something went wrong!",
    };

    // if (err instanceof CustomApiError) {
    //     return res.status(err.statusCode).json({
    //         status: err.status,
    //         message: err.message,
    //     });
    // }

    if (err.name && err.name === "ValidationError") {
        customError.statusCode = StatusCodes.NOT_FOUND;
        customError.message = Object.keys(err.errors)
            .map((item) => `${item} is mandatory`)
            .join(",");
    }

    if (err.code && err.code === 11000) {
        customError.statusCode = StatusCodes.BAD_REQUEST;
        customError.message = `Duplicate value entered for keys ${Object.keys(err.keyValue)} field`;
    }

    if (err.name && err.name === "CastError") {
        customError.statusCode = StatusCodes.NOT_FOUND;
        customError.message = `No item found with id ${err.value}`;
    }

    return res.status(customError.statusCode).json({
        status: customError.status,
        message: customError.message,
    });
};
