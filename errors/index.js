const { BadRequestError } = require("./BadRequestError");
const { CustomApiError } = require("./CustomApiError");
const { NotFoundError } = require("./NotFoundError");
const { UnAuthenticatedError } = require("./UnAuthenticatedError");

module.exports = {
    BadRequestError,
    CustomApiError,
    NotFoundError,
    UnAuthenticatedError,
};
