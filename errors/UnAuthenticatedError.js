const { CustomApiError } = require("./CustomApiError");
const { StatusCodes } = require("http-status-codes");

class UnAuthenticatedError extends CustomApiError {
    constructor(message) {
        super(message);
        this.status = false;
        this.statusCode = StatusCodes.UNAUTHORIZED;
    }
}

module.exports = { UnAuthenticatedError };
