const { CustomApiError } = require("./CustomApiError");
const { StatusCodes } = require("http-status-codes");

class NotFoundError extends CustomApiError {
    constructor(message) {
        super(message);
        this.status = false;
        this.statusCode = StatusCodes.NOT_FOUND;
    }
}

module.exports = { NotFoundError };
